var rpf = require("../build/librpf.js");
// Create some binary maps
var exmap = new rpf.BinaryMap(),
	immap = new rpf.BinaryMap();

var secondary1 = new rpf.Node("secondary1");
secondary1.push(new rpf.String("Hello there"));
secondary1.push(new rpf.Int(4294961295 - (0xFFFFFFFF + 1))); // This is an unsigned value but JS doesn't like it.
secondary1.push(new rpf.Int(4294961295 - (0xFFFFFFFF + 1))); // Keep things compatible with the CPP version
exmap.getRoot().push(secondary1);
var secondary2 = new rpf.Node();
secondary2.push(new rpf.Float(3.14159));
secondary2.push(new rpf.String("Hello again"));
secondary2.push(new rpf.Byte(6));
exmap.getRoot().push(secondary2);
var tertiary1 = new rpf.Node("tertiary1");
tertiary1.push(new rpf.Float(14.155299));
secondary2.push(tertiary1);

console.log("Here is what you saved: " + exmap.getRoot().at("secondary1").at(0).getValue());
console.log("Here is what you saved: " + exmap.getRoot().at("secondary1").at(1).getValue());
console.log("Here is what you saved: " + exmap.getRoot().at(1).at(0).getValue());
console.log("Here is what you saved: " + exmap.getRoot().at(1).at(1).getValue());
console.log("Here is what you saved: " + exmap.getRoot().at(1).at(2).getValue());
console.log("Here is what you saved: " + exmap.getRoot().at(1).at("tertiary1").at(0).getValue());

exmap.exportFileSync("javascript-exported.rpf");

immap.importFileSync("javascript-exported.rpf");
console.log("Here is what I read (file): " + immap.getRoot().at("secondary1").at(0).getValue());
console.log("Here is what I read (file): " + immap.getRoot().at("secondary1").at(1).getValue());
console.log("Here is what I read (file): " + immap.getRoot().at(1).at(0).getValue());
console.log("Here is what I read (file): " + immap.getRoot().at(1).at(1).getValue());
console.log("Here is what I read (file): " + immap.getRoot().at(1).at(2).getValue());
console.log("Here is what I read (file): " + immap.getRoot().at(1).at("tertiary1").at(0).getValue());
immap.destroy();

immap = new rpf.BinaryMap();
immap.import(exmap.export());

console.log("Here is what I read (raw): " + immap.getRoot().at("secondary1").at(0).getValue());
console.log("Here is what I read (raw): " + immap.getRoot().at("secondary1").at(1).getValue());
console.log("Here is what I read (raw): " + immap.getRoot().at(1).at(0).getValue());
console.log("Here is what I read (raw): " + immap.getRoot().at(1).at(1).getValue());
console.log("Here is what I read (raw): " + immap.getRoot().at(1).at(2).getValue());
console.log("Here is what I read (raw): " + immap.getRoot().at(1).at("tertiary1").at(0).getValue());
immap.destroy();

exmap.destroy();

immap = new rpf.BinaryMap();
immap.importFileSync("cpp-exported.rpf");
console.log("Here is what I read (compat): " + immap.getRoot().at("secondary1").at(0).getValue());
console.log("Here is what I read (compat): " + immap.getRoot().at("secondary1").at(1).getValue());
console.log("Here is what I read (compat): " + immap.getRoot().at(1).at(0).getValue());
console.log("Here is what I read (compat): " + immap.getRoot().at(1).at(1).getValue());
console.log("Here is what I read (compat): " + immap.getRoot().at(1).at(2).getValue());
console.log("Here is what I read (compat): " + immap.getRoot().at(1).at("tertiary1").at(0).getValue());
immap.destroy();