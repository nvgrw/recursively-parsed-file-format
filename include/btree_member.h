/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>

#include "btree_exceptions.h"
#include "BUtil.h"

using namespace std;

#define BTREE_HEADER_LENGTH 9
#define BTREE_TYPE_SPECIFIC_LENGTH 4
#define BTREE_NODE_SKIP 9
#define BTREE_MAGIC_NUMBERS 9
#define BTREE_MAGIC_NUMBER_LIT 0x52, 0x50, 0x46, 0x4e, 0x4f, 0x54, 0x52, 0x4f
#define BTREE_REVISION 2

namespace RPF {
    namespace btree_type{
        const static unsigned char NODE = 1;
        const static unsigned char BYTE = 2;
        const static unsigned char INT = 3;
        const static unsigned char STRING = 4;
        const static unsigned char FLOAT = 5;
    };
    
    class btree_member;
    typedef vector<btree_member*> node_t;
    
    class btree_member {
    public:
        virtual ~btree_member(){}
        
        virtual unsigned char getType() = 0;
        virtual unsigned int getLength() = 0;
        virtual unsigned int typeSpecific() = 0;
        virtual vector<unsigned char> getAsBytes() = 0;
    protected:
        virtual node_t* getNODEValue(){return NULL;}
        virtual unsigned char* getUBYTEValue(){return NULL;}
        virtual char* getBYTEValue(){return NULL;}
        virtual unsigned int* getUINTValue(){return NULL;}
        virtual int* getINTValue(){return NULL;}
        virtual string* getSTRINGValue(){return NULL;}
        virtual float* getFLOATValue(){return NULL;}
        
        virtual void setNODEValue(void*){}
        virtual void setUBYTEValue(void*){}
        virtual void setBYTEValue(void*){}
        virtual void setUINTValue(void*){}
        virtual void setINTValue(void*){}
        virtual void setSTRINGValue(void*){}
        virtual void setFLOATValue(void*){}
        
        vector<unsigned char> getHeaderBytes(){
            vector<unsigned char> m;
            m.push_back(getType());
            vector<unsigned char> split = BUtil::splitNum(getLength());
            for(unsigned char byte:split){
                m.push_back(byte);
            }
            if(getType() == btree_type::NODE || getType() == (btree_type::NODE | 0x80)){
                split = BUtil::splitNum(typeSpecific());
                for(unsigned char byte:split){
                    m.push_back(byte);
                }
            }
            return m;
        }
    public:
        template<typename T> T* getValue(){
            unsigned char type = getType();
            if(type == btree_type::NODE || type == (btree_type::NODE | 0x80))
                return (T*)getNODEValue();
            if(type == btree_type::BYTE)
                return (T*)getBYTEValue();
            if(type == (btree_type::BYTE | 0x80))
                return (T*)getUBYTEValue();
            if(type == btree_type::INT)
                return (T*)getINTValue();
            if(type == (btree_type::INT | 0x80))
                return (T*)getUINTValue();
            if(type == btree_type::STRING)
                return (T*)getSTRINGValue();
            if(type == btree_type::FLOAT)
                return (T*)getFLOATValue();
            
            throw invalid_btree_type_exception(type);
        }
        
        template<typename T> void setValue(T val){
            string type = typeid(T).name();
            T* tPtr = &val;
            if(type == typeid(node_t).name()){
                setNODEValue((void*)tPtr);
                return;
            }
            if(type == typeid(char).name()){
                setBYTEValue((void*)tPtr);
                return;
            }
            if(type == typeid(unsigned char).name()){
                setUBYTEValue((void*)tPtr);
                return;
            }
            if(type == typeid(int).name()){
                setINTValue((void*)tPtr);
                return;
            }
            if(type == typeid(unsigned int).name()){
                setUINTValue((void*)tPtr);
                return;
            }
            if(type == typeid(string).name()){
                setSTRINGValue((void*)tPtr);
                return;
            }
            if(type == typeid(float).name()){
                setFLOATValue((void*)tPtr);
                return;
            }
            
            throw invalid_btree_type_exception(0);
        }
    };
}