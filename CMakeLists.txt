cmake_minimum_required (VERSION 2.6)
set(CMAKE_CXX_COMPILER g++)
project(librpf)
include_directories("include")
file(GLOB librpf_SRC
    "src/*.cpp"
    "include/*.h"
)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_library(librpf STATIC ${librpf_SRC})
set_target_properties(librpf PROPERTIES LIBRARY_OUTPUT_NAME rpf)
set_target_properties(librpf PROPERTIES OUTPUT_NAME rpf)