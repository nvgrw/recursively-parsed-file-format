/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "btree_int.h"

RPF::btree_int::btree_int(int value):value(value){
    
}

RPF::btree_int::btree_int(unsigned int value):value(value){
    snd = false;
}

RPF::btree_int::~btree_int(){
    
}

unsigned char RPF::btree_int::getType(){
    return snd ? btree_type::INT : (btree_type::INT | 0x80);
}

unsigned int RPF::btree_int::getLength(){
    unsigned int length = (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH);
    length += 4;
    return length;
}

unsigned int RPF::btree_int::typeSpecific(){
    return 0;
}

vector<unsigned char> RPF::btree_int::getAsBytes(){
    vector<unsigned char> m = getHeaderBytes();
    vector<unsigned char> split = BUtil::splitNum(value);
    for(unsigned char byte:split){
        m.push_back(byte);
    }
    return m;
}

int* RPF::btree_int::getINTValue(){
    return &value;
}

unsigned int* RPF::btree_int::getUINTValue(){
    return reinterpret_cast<unsigned int*>(&value);
}

void RPF::btree_int::setINTValue(void* val){
    *(&value) = *((int*)val);;
    snd = true;
}

void RPF::btree_int::setUINTValue(void* val){
    *(&value) = *((unsigned int*)val);
    snd = false;
}