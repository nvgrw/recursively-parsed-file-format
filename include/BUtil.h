/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <iostream>
#include <vector>

#include "btree_exceptions.h"

using namespace std;

namespace RPF {
    class BUtil{
    public:
        BUtil();
        ~BUtil();
        
        template<typename T = int32_t>
        static vector<unsigned char> splitNum(T num){
            size_t size = sizeof(T);
            unsigned char isolator = 0xFF;
            vector<unsigned char> e;
            for(int i = 0; i < size; i++){
                size_t rel = size - i - 1;
                T movedIsolator = isolator << 8 * rel;
                e.push_back((num & movedIsolator) >> 8 * rel);
            }
            return e;
        }
        
        template<typename T = int32_t>
        static T joinNum(vector<unsigned char> bytes){
            size_t size = sizeof(T);
            T e = 0;
            for(int i = 0; i < size; i++){
                size_t rel = size - i - 1;
                T unisolated = bytes[i] << 8 * rel;
                e |= unisolated;
            }
            return e;
        }
        
        static void safeErase(vector<unsigned char>& data, vector<unsigned char>::const_iterator first, vector<unsigned char>::const_iterator last);
    };
}