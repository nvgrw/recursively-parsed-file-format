/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "BImporter.h"

RPF::BImporter::BImporter(const BinaryMap& map):bmap(map){
    
}

RPF::BImporter::~BImporter(){
    
}

void RPF::BImporter::loadFile(string dir){
    ifstream in(dir, ios::binary);
    if(in.is_open()){
        in.seekg(0, in.end);
        unsigned int length = (unsigned int)in.tellg();
        in.seekg(0, in.beg);
        
        char* buffer = new char[length];
        in.read(buffer, length);
        vector<unsigned char> readData;
        for(unsigned int i = 0; i < length; i++){
            readData.push_back((unsigned char)buffer[i]);
        }
        loadRaw(readData);
        in.close();
    }else{
        throw runtime_error(string("Failed to open file for import: ").append(dir));
    }
}

void RPF::BImporter::loadRaw(vector<unsigned char> data){
    try{
        btree_node* root = const_cast<BinaryMap&>(bmap).getRoot();
        
        // Check if magic numbers & revision match
        vector<unsigned char> magicNos = {BTREE_MAGIC_NUMBER_LIT, BTREE_REVISION};
        for(int i = 0; i < BTREE_MAGIC_NUMBERS; i++){
            if(data[i] == magicNos[i]) continue;
            if(i == BTREE_MAGIC_NUMBERS - 1){
                if(data[i] == magicNos[i]) continue;
                throw time_travel_exception();
            }
            throw corrupted_btree_exception();
        }
        
        BUtil::safeErase(data, data.begin(), data.begin() + BTREE_MAGIC_NUMBERS);
        header_t rootHeader = parseHeader(data);
        BUtil::safeErase(data, data.begin(), data.begin() + BTREE_NODE_SKIP);
        parseNODE(root, rootHeader, data, true);
    }catch(out_of_range){
        throw corrupted_btree_exception();
    }catch(length_error){
        throw corrupted_btree_exception();
    }catch(bad_alloc){
        throw corrupted_btree_exception();
    }
}

void RPF::BImporter::parseTYPE(btree_node* parent, vector<unsigned char> trimmedData){
    header_t header = parseHeader(trimmedData);
    unsigned char type = get<HeaderIndex::TYPE>(header);
    if(trimmedData.size() != get<HeaderIndex::LENGTH>(header)){
        BUtil::safeErase(trimmedData, trimmedData.begin() + get<HeaderIndex::LENGTH>(header), trimmedData.end()); // Erase everything beyond
    }
    
    if(type == btree_type::NODE || type == (btree_type::NODE | 0x80)){
        BUtil::safeErase(trimmedData, trimmedData.begin(), trimmedData.begin() + BTREE_HEADER_LENGTH); // Erase header with specific
    }else{
        BUtil::safeErase(trimmedData, trimmedData.begin(), trimmedData.begin() + (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH)); // Erase header without specific
    }
    
    if(type == btree_type::NODE || type == (btree_type::NODE | 0x80)){
        parseNODE(parent, header, trimmedData);
        return;
    }
    if(type == btree_type::BYTE){
        parseBYTE(parent, header, trimmedData);
        return;
    }
    if(type == (btree_type::BYTE | 0x80)){
        parseUBYTE(parent, header, trimmedData);
        return;
    }
    if(type == btree_type::INT){
        parseINT(parent, header, trimmedData);
        return;
    }
    if(type == (btree_type::INT | 0x80)){
        parseUINT(parent, header, trimmedData);
        return;
    }
    if(type == btree_type::STRING){
        parseSTRING(parent, header, trimmedData);
        return;
    }
    if(type == btree_type::FLOAT){
        parseFLOAT(parent, header, trimmedData);
        return;
    }
    
    throw invalid_btree_type_exception(type);
}

void RPF::BImporter::parseNODE(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData, bool parentIsSelf){
    unsigned int children = get<HeaderIndex::SPECIFIC>(header);
    btree_node* thisNode = parent;
    if(!parentIsSelf){ // n.b. If the parent is self (root) then we start at BTREE_NODE_SKIP!
        if(get<HeaderIndex::TYPE>(header) == btree_type::NODE){
            // Let's read the identifier
            header_t identifierHeader = parseHeader(trimmedData);
            BUtil::safeErase(trimmedData, trimmedData.begin(), trimmedData.begin() + (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH));
            stringstream ss;
            for(unsigned int i = 0; i < get<HeaderIndex::LENGTH>(identifierHeader) - (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH); i++){
                ss << (char)trimmedData[i];
            }
            thisNode = new btree_node(ss.str());
            BUtil::safeErase(trimmedData, trimmedData.begin(), trimmedData.begin() + get<HeaderIndex::LENGTH>(identifierHeader) - (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH));
        }else if(get<HeaderIndex::TYPE>(header) == (btree_type::NODE | 0x80)){
            thisNode = new btree_node();
        }
        parent->push_back(thisNode);
    }
    
    // Read the rest
    for(unsigned int i = 0; i < children; i++){
        header_t childHeader = parseHeader(trimmedData);
        parseTYPE(thisNode, trimmedData);
        BUtil::safeErase(trimmedData, trimmedData.begin(), trimmedData.begin() + get<HeaderIndex::LENGTH>(childHeader));
    }
}

void RPF::BImporter::parseUBYTE(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    parent->push_back(new btree_byte((unsigned char)trimmedData[0]));
}

void RPF::BImporter::parseBYTE(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    parent->push_back(new btree_byte((char)trimmedData[0]));
}

void RPF::BImporter::parseUINT(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    unsigned int bv = BUtil::joinNum({trimmedData[0], trimmedData[1], trimmedData[2], trimmedData[3]});
    parent->push_back(new btree_int(bv));
}

void RPF::BImporter::parseINT(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    int bv = BUtil::joinNum({trimmedData[0], trimmedData[1], trimmedData[2], trimmedData[3]});
    parent->push_back(new btree_int(bv));
}

void RPF::BImporter::parseSTRING(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    stringstream ss;
    for(unsigned int i = 0; i < get<HeaderIndex::LENGTH>(header) - (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH); i++){
        ss << (char)trimmedData[i];
    }
    parent->push_back(new btree_string(ss.str()));
}

void RPF::BImporter::parseFLOAT(btree_node* parent, header_t& header, vector<unsigned char>& trimmedData){
    unsigned int bv = BUtil::joinNum({trimmedData[0], trimmedData[1], trimmedData[2], trimmedData[3]});
    parent->push_back(new btree_float(*reinterpret_cast<float*>(&bv)));
}

RPF::BImporter::header_t RPF::BImporter::parseHeader(vector<unsigned char>& data){
    header_t parsedHeader;
    
    get<HeaderIndex::TYPE>(parsedHeader) = data[0]; // Type
    get<HeaderIndex::LENGTH>(parsedHeader) = BUtil::joinNum({data[1], data[2], data[3], data[4]}); // Length
    if(get<HeaderIndex::TYPE>(parsedHeader) == btree_type::NODE || (btree_type::NODE | 0x80)){
        get<HeaderIndex::SPECIFIC>(parsedHeader) = BUtil::joinNum({data[5], data[6], data[7], data[8]}); // Type Specific
    }else{
        get<HeaderIndex::SPECIFIC>(parsedHeader) = (int)0;
    }
    
    return parsedHeader;
}