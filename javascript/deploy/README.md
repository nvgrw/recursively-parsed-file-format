# librpf
librpf.js is a port of my RPF library (originally written in C++) to JavaScript using [emscripten](http://emscripten.org) and a little C API wrapper thing.
It's pretty neat and can do pretty much everything that my RPF library can do apart from type signedness because \*cough\* JavaScript \*cough\*.

## Setup
### The easy way
There are precompiled versions available (and even a version on NPM!), so if you don't want to build everything
yourself just add `"librpf" : "1.2.0"` to your dependencies or run `npm install librpf` in your project directory. Or just copy the librpf.js file from the build directory!

### The hard way
So you want to build the library? It's not actually that hard.

#### What you need
- An installation of [node.js](http://nodejs.org/)
- An installation of [emscripten](http://emscripten.org/)
- Some free time and general command-line know-how. If you use Windows, bear in mind that the steps are similar, but certain things are run differently.

#### The steps

1. First, `cd` to this (the `./javascript`) directory.
2. Next, run `npm install` to install the dependencies of the build tool.
3. Now open up `build.js` with your favourite text editor and edit the variables. Most notably, you will have to edit the path of your emscripten SDK directory and the path to your `em++` executable, relative to the path specified previously. If you fancy changing the deployment or export directory names and library names, that option's there too.
4. Run `./build.js build` to build the library. The script should clean up before this process and after this process, so don't worry about the files that are crated during the compilation process. Once building is done, the library will be minified and placed in the specified build directory. If you'd like to skip minification, set `minify = false` in the build script.
    + __Note:__ you can also 'deploy' the library - basically a copy of the script and the a package.json file are created in a directory specified in the build script. To do this, run `build.js` again, this time as `./build.js deploy`. If you'd like to build and then deploy directly, you can also run `./build.js all`.
5. At this point you should be done!


## Using the library
Make sure to read the API tutorial given in the README file of the C++ version of this library to get an understanding of how RPF works and then come back to read about how I've implemented this in JavaScript below.

The library should work both in browsers and node.js - keep in mind however that you can't write files in the browser version and only im/export data in the form of an array.

For browser users, the API is hidden within the `rpf` object. Obviously for node.js you write `var rpf = require("librpf");`

A comprehensive example, very similar to the C++ example, is located within the `test.js` file.

### Writing data
#### Creating a BinaryMap
Create a BinaryMap object. Once you're done using it, destroy it in order to free memory.
```javascript
var myBinaryMap = new BinaryMap();
// ...
myBinaryMap.destroy();
```

#### Pushing to a BinaryMap
To add something to a BinaryMap, push something onto its root object.
```javascript
myBinaryMap.getRoot().push(new rpf.String("My awesome string"));
```
Available types are:
```javascript
rpf.Node
rpf.String
rpf.Byte
rpf.Int
rpf.Float
```
Each non-node type takes the initial value as its first argument when initialising the object.
Nodes can be initialised without an argument to become anonymous (no identifier) or with a string
that acts as its identifier which helps to make it more identifiable.

It must be noted that as soon as you attach a BtreeMember such as a String, Int, etc&hellip; to a BinaryMap,
that BinaryMap takes full ownership over the memory of your object. That means that to destroy your
BinaryMap you will only have to call destroy on it and the BinaryMap will delete the memory of all of
its children. That does, however mean that you can only attach one data object once, not multiple times.

#### Erasing a value from a BinaryMap
To erase a value from a BinaryMap, you will need to know its exact location. You can erase by index
and by identifier (for nodes).
```javascript
myBinaryMap.getRoot().at(0).at("someIdentifier").at(44).erase(0);
```
Of course the above example is exaggerated, but it illustrates the tree structure of a BinaryMap.
This code would delete the (hypothetical) BtreeMember at index 0 of the node located at index 44 of
the node identified by "someIdentifier" located at index 0 of the root node.

#### Exporting

##### Arrays
To export to an array;
```javascript
var arrayOfExportedData = myBinaryMap.export();
```

##### Files
Exporting of files is only supported when you're using the Node.js module!

Async:
```javascript
myBinaryMap.exportFile("file.rpf", function(err){
    // Handle errors here
});
```

Sync:
```javascript
myBinaryMap.exportFileSync("file.rpf");
```
It's as simple as that! Importing will be covered in the 'Reading data' section.

### Reading Data
#### Getting a value of a member
Unlike the C++ version, you will not need to provide the data type of the value you are trying to read.
```javascript
var storedValue = myBinaryMap.at(0).at("someIdentifier").at(44).at(0).getValue();
```
Again the location of this value is hypothetical, but again it demonstrates the ease at which you can traverse
the tree. This code would return the value of a BtreeMember that is at index 0 of a node located at index 44 of a
node identified by "someIdentifier" located at index 0 of the root of our BinaryMap. Nice.

#### Importing
##### Arrays
To import from an array;
```javascript
myBinaryMap.import(array);
```

##### Files
Importing of files is only supported when you're using the Node.js module!

Async:
```javascript
myBinaryMap.importFile("file.rpf", function(err){
    // Handle errors here
});
```

Sync:
```javascript
myBinaryMap.importFileSync("file.rpf");
```

## Extra Stuff
If you fancy messing around with the library and internals and know what you're doing, I have exported
all cwrapped C functions as well as the emscripten module itself. All functions are at `rpf.internal`
and the module is at `rpf.internal.library`. Have fun!

## Specification
For that, please check the main README on this repository.