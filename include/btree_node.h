/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "btree_member.h"
#include "btree_string.h"
#include <assert.h>

using namespace std;

#define BTREE_ANONYMOUS_NODE_IDENTIFIER "_n_anonymous"

namespace RPF {
    class btree_node:
        public btree_member{
    public:
        btree_node();
        btree_node(string identifier);
        ~btree_node();
            
        string getIdentifier();
        void setIdentifier(string newID);
            
        void push_back(btree_member* member);
        void pop_back();
            
        node_t::iterator begin();
        node_t::const_iterator begin() const;
        node_t::iterator end();
        node_t::const_iterator end() const;
        node_t::iterator rbegin();
        node_t::const_iterator rbegin() const;
        node_t::iterator rend();
        node_t::const_iterator rend() const;
        node_t::reference front();
        node_t::const_reference front() const;
        node_t::reference back();
        node_t::const_reference back() const;
            
        // You should use these functions over those provided by the vector as these will also delete them beforehand!
        node_t::iterator erase(node_t::const_iterator position);
        node_t::iterator erase(node_t::const_iterator first, node_t::const_iterator last);
        node_t::iterator erase(string key);
        node_t::iterator erase(size_t index);
            
        size_t size();
        
        // Square brackets
        template<typename T = btree_node>
        T* operator[](size_t loc){
            return (T*)members.at(loc);
        }
            
        btree_node* operator[](string key);
            
        // at
        template<typename T = btree_node>
        T* at(size_t loc){
            return (T*)members.at(loc);
        }
            
        btree_node* at(string key);
        
        // Overridden
        unsigned char getType();
        unsigned int getLength();
        unsigned int typeSpecific();
        vector<unsigned char> getAsBytes();
          
    protected:
        node_t* getNODEValue();
        void setNODEValue(void* val);
    private:
        string identifier;
        node_t members;
    };
}