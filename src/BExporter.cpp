/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "BExporter.h"

RPF::BExporter::BExporter(const BinaryMap& map):bmap(map){
    
}

RPF::BExporter::~BExporter(){
    
}

void RPF::BExporter::saveFile(string dir){
    vector<unsigned char> bytes = ((btree_node*)const_cast<BinaryMap&>(bmap).getRoot())->getAsBytes();
    addMagicNumbers(bytes);
    ofstream out(dir);
    if(out.is_open()){
        for(unsigned char chr:bytes){
            out << chr;
        }
        out.close();
    }else{
        throw runtime_error(string("Failed to open file for export: ").append(dir));
    }
}

vector<unsigned char> RPF::BExporter::saveRaw(){
    try{
        vector<unsigned char> bytes = ((btree_node*)const_cast<BinaryMap&>(bmap).getRoot())->getAsBytes();
        addMagicNumbers(bytes);
        return bytes;
    }catch(...){
        cout << "RPF Raw Export Failed for an unknown reason.\n";
        vector<unsigned char> bytes;
        return bytes;
    }
}

void RPF::BExporter::addMagicNumbers(vector<unsigned char>& data){
    data.insert(data.begin(), {BTREE_MAGIC_NUMBER_LIT, BTREE_REVISION});
}