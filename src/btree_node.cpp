/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "btree_node.h"

RPF::btree_node::btree_node():identifier(BTREE_ANONYMOUS_NODE_IDENTIFIER){
    
}

RPF::btree_node::btree_node(string identifier):identifier(identifier){
    if(identifier.length() == 0) identifier = BTREE_ANONYMOUS_NODE_IDENTIFIER;
}

RPF::btree_node::~btree_node(){
    for(btree_member* m:members){
        delete m;
    }
    members.clear();
}

string RPF::btree_node::getIdentifier(){
    return identifier;
}

void RPF::btree_node::setIdentifier(string newID){
    if(newID.length() == 0) identifier = BTREE_ANONYMOUS_NODE_IDENTIFIER;
    else
    identifier = newID;
}

void RPF::btree_node::push_back(RPF::btree_member* member){
    members.push_back(member);
}

void RPF::btree_node::pop_back(){
    if(members.size() > 0){
        delete members.at(members.size() - 1);
        members.pop_back();
    }
}

RPF::node_t::iterator RPF::btree_node::begin(){
    return members.begin();
}

RPF::node_t::const_iterator RPF::btree_node::begin() const{
    return members.begin();
}

RPF::node_t::iterator RPF::btree_node::end(){
    return members.end();
}

RPF::node_t::const_iterator RPF::btree_node::end() const{
    return members.end();
}

RPF::node_t::iterator RPF::btree_node::rbegin(){
    return members.begin();
}

RPF::node_t::const_iterator RPF::btree_node::rbegin() const{
    return members.begin();
}

RPF::node_t::iterator RPF::btree_node::rend(){
    return members.end();
}

RPF::node_t::const_iterator RPF::btree_node::rend() const{
    return members.end();
}

RPF::node_t::reference RPF::btree_node::front(){
    return members.front();
}

RPF::node_t::const_reference RPF::btree_node::front() const{
    return members.front();
}

RPF::node_t::reference RPF::btree_node::back(){
    return members.back();
}

RPF::node_t::const_reference RPF::btree_node::back() const{
    return members.back();
}

RPF::node_t::iterator RPF::btree_node::erase(node_t::const_iterator position){
    size_t index = position - members.begin();
    delete members.at(index);
    return members.erase(position);
}

RPF::node_t::iterator RPF::btree_node::erase(node_t::const_iterator first, node_t::const_iterator last){
    size_t firstI = first - members.begin();
    size_t lastI = last - members.begin();
    size_t diff = lastI - firstI;
    for(unsigned int i = 0; i < diff; i++){
        delete members.at(i + firstI);
    }
    return members.erase(first, last);
}

RPF::node_t::iterator RPF::btree_node::erase(string key){
    for(unsigned int i = 0; i < members.size(); i++){
        btree_member* m = members.at(i);
        if(m->getType() == btree_type::NODE){
            if(((btree_node*)m)->getIdentifier() == key){
                return erase(members.begin() + i);
            }
        }
    }
    return members.end();
}

RPF::node_t::iterator RPF::btree_node::erase(size_t index){
    return erase(members.begin() + index);
}

size_t RPF::btree_node::size(){
    return members.size();
}

RPF::btree_node* RPF::btree_node::operator[](string key){
    for(btree_member* m:members){
        if(m->getType() == btree_type::NODE){
            if(((btree_node*)m)->getIdentifier() == key){
                return (btree_node*)m;
            }
        }
    }
    return NULL;
}

RPF::btree_node* RPF::btree_node::at(string key){
    return operator[](key);
}

unsigned char RPF::btree_node::getType(){
    return string(BTREE_ANONYMOUS_NODE_IDENTIFIER) != identifier ? btree_type::NODE : btree_type::NODE | 0x80;
}

unsigned int RPF::btree_node::getLength(){
    unsigned int length = BTREE_HEADER_LENGTH;
    if(string(BTREE_ANONYMOUS_NODE_IDENTIFIER) != identifier){
        btree_string id(identifier);
        length += id.getLength();
    }
    for(unsigned int i = 0; i < members.size(); i++){
        length += members[i]->getLength();
    }
    return length;
}

unsigned int RPF::btree_node::typeSpecific(){
    return (unsigned int)members.size();
}

vector<unsigned char> RPF::btree_node::getAsBytes(){
    vector<unsigned char> m = getHeaderBytes();
    if(string(BTREE_ANONYMOUS_NODE_IDENTIFIER) != identifier){
        btree_string id(identifier);
        vector<unsigned char> idAsBytes = id.getAsBytes();
        for(unsigned char chr:idAsBytes){
            m.push_back(chr);
        }
    }
    for(btree_member* member:members){
        vector<unsigned char> memberAsBytes = member->getAsBytes();
        for(unsigned char chr:memberAsBytes){
            m.push_back(chr);
        }
    }
    return m;
}

RPF::node_t* RPF::btree_node::getNODEValue(){
    return &members;
}

// I'd be careful with this one and rather re-create the node.
void RPF::btree_node::setNODEValue(void* val){
    *(&members) = *((node_t*)val);
}