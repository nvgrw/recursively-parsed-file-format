#!/usr/local/bin/node
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var fs = require("fs"),
	path = require("path"),
	sh = require("execSync"),
	rm = require("remove").removeSync,
	uglify = require("uglify-js");

// Change the values below to your liking
var EM_DIR = "/Volumes/MacintoshHD/notrodash/Development/emsdk_portable",
	EMPP_DIR = EM_DIR + "/emscripten/1.16.0/em++",
	LIB_EXPORT_DIR = "./build",
    LIB_NAME = "librpf.js";
	LIB_EXPORT_NAME = LIB_EXPORT_DIR + "/" + LIB_NAME,
	LIB_DEPLOY_DIR = "./deploy",
	LIB_DEPLOY_NAME = LIB_DEPLOY_DIR + "/" + LIB_NAME,
	EXPORT_FUN = "EXPORTED_FUNCTIONS=\"" + aggregateFunctionNames() + "\"",
	OPTIMISATIONS = "-O2",
	ASSERTIONS = 0,
	DISABLE_EXCEPTION_CATCHING = 1,
	minify = true,
	version = "1.2.1"; // version.revision.hotfix

function aggregateFunctionNames(){
	var wrapperFile = fs.readFileSync("src/EmscriptenWrapper.cpp", {encoding:"utf8"});
	var funNames = wrapperFile.match(/\s+([A-Za-z_]+)\((.*){/g);
	var formatted = "[";
	for(var i = 0; i < funNames.length; i++){
		var funName;
		if(i < funNames.length - 1)
			funName = funNames[i].replace(/\s+([A-Za-z_]+)\((.*){/g, "'_$1',");
		else
			funName = funNames[i].replace(/\s+([A-Za-z_]+)\((.*){/g, "'_$1'");
		formatted += funName;
	}
	formatted += "]";
	return formatted;
}

function traverse(dir, regex, itemOperation, basename){
	basename = basename || false;
	var files = fs.readdirSync(dir);
	if(files != null){
		for(var i = 0; i < files.length; i++){
			var fileMatch = files[i].match(regex);
			if(fileMatch!=null && fileMatch.length > 0){
				if(!basename){
					itemOperation(path.join(dir, fileMatch[0]));
				}else{
					itemOperation(path.join(dir, fileMatch[0]), fileMatch[0]);
				}
			}
		}
	}
}

function copy(old, neww){
	var olds = fs.createReadStream(old);
	var news = fs.createWriteStream(neww);
	olds.pipe(news);
}

var command = process.argv[2] || "";
switch(command){
	case "build":
	default: {
		build();
	}
		break;
	case "deploy": {
		deploy();
	}
		break;
	case "all": {
		build();
		deploy();
	}
}

function build(){
	console.log("Cleaning up...");
	traverse("./", /(.*)\.cpp\.o/g, rm);
	if(fs.existsSync(LIB_EXPORT_DIR))rm(LIB_EXPORT_DIR);
	fs.mkdirSync(LIB_EXPORT_DIR);

	traverse("../src/", /(.*)\.cpp/g, function(path, basename){
		console.log("Compiling source " + path);
		sh.run(EMPP_DIR + " " + OPTIMISATIONS + " -std=c++11 -I../include " + path + " -s ASSERTIONS=" + ASSERTIONS + " -s DISABLE_EXCEPTION_CATCHING=" + DISABLE_EXCEPTION_CATCHING + " -o ./" + basename + ".o;");
	}, true);

	console.log("Compiling source ./src/EmscriptenWrapper.cpp");
	sh.run(EMPP_DIR + " " + OPTIMISATIONS + " -std=c++11 -I../include ./src/EmscriptenWrapper.cpp -s " + EXPORT_FUN + " -s ASSERTIONS=" + ASSERTIONS + " -s DISABLE_EXCEPTION_CATCHING=" + DISABLE_EXCEPTION_CATCHING + " -o ./EmscriptenWrapper.cpp.o;");

	var objects = "";
	traverse("./", /(.*)\.cpp\.o/g, function(path){
		objects += path;
		objects += " ";
	});
	objects = objects.trim();

	console.log("Compiling bitcode...");
	sh.run(EMPP_DIR + " " + OPTIMISATIONS + " " + objects + " -s ASSERTIONS=" + ASSERTIONS + " -s " + EXPORT_FUN + " -s DISABLE_EXCEPTION_CATCHING=" + DISABLE_EXCEPTION_CATCHING + " -o " + LIB_EXPORT_NAME + ";")

	console.log("Cleaning up...");
	traverse("./", /(.*)\.cpp\.o/g, rm);

	console.log("Finishing closure...");
	var library = "var librpf = (function(){" + fs.readFileSync(LIB_EXPORT_NAME, {encoding:"utf8"}) + "\n;return Module;})();";
	// I had to split the wrapper into two parts because JavaScript's replace did not like the emscripten code and I was too lazy to split it in code.
	var wrapper = fs.readFileSync("./src/wrapper.part1.js", {encoding:"utf8"}) + library + fs.readFileSync("./src/wrapper.part2.js", {encoding:"utf8"});
	rm(LIB_EXPORT_NAME);

	if(minify){
		console.log("Minifying...");
		wrapper = uglify.minify(wrapper, {fromString:true}).code;
	}
	console.log("Writing library...");
	fs.writeFileSync(LIB_EXPORT_NAME, wrapper);

	console.log("Done! Find the created JavaScript file in " + LIB_EXPORT_DIR);
}

function deploy(){
	console.log("Cleaning up...");
	if(fs.existsSync(LIB_DEPLOY_DIR)) rm(LIB_DEPLOY_DIR);
	fs.mkdirSync(LIB_DEPLOY_DIR);

	if(!fs.existsSync(LIB_EXPORT_NAME)){
		console.log("Cannot find RPF library for deployment! Perhaps you forgot to build it.");
		process.exit(1);
	}

	var pkgInfo = {
	    "author": "Niklas Vangerow <n@notr.co> (http://notrodash.co.uk/)",
	    "name": "librpf",
	    "description": "A library for the RPF file format, revision 2",
	    "version": version,
	    "license": "MIT",
	    "repository": {
	    	"type" : "git",
	    	"url" : "https://github.com/notrodash/recursively-parsed-file-format.git"
	    },
	    "main" : LIB_NAME
	};
	console.log("Creating package.json...");
	fs.writeFileSync(path.join(LIB_DEPLOY_DIR, "package.json"), JSON.stringify(pkgInfo));

	console.log("Copying library...");
	copy(LIB_EXPORT_NAME, LIB_DEPLOY_NAME);

    console.log("Copying readme...");
    copy("./README.md", path.join(LIB_DEPLOY_DIR, "README.md"));

	console.log("Done! Find the deployed, npm ready files in " + LIB_DEPLOY_DIR);
}