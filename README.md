# RPF: Recursively Parsed File Format
RPF is a file format specification and this repository contains a library that can both write and read RPF files. It's written in C++, so it works with many things already out there and I have provided CMakeLists files to make compilation go without a hitch.

__Important:__ Unless you write a converter connecting the RPF 1.0 library up with the RPF 2.0 library, the formats are not compatible as they differ in specification.

## The specification
There are 5 data types:

| Type    | Signed | Unsigned | Mask   |
| ------- | :----: | :------: | :----: |
| NODE    | n/a    | n/a      | 0x01   |
| BYTE    | yes    | yes      | 0x02   |
| INT     | yes    | yes      | 0x03   |
| STRING  | n/a    | n/a      | 0x04   |
| FLOAT   | n/a    | n/a      | 0x05   |

The file format consists of "type headers" which are comprised of 5 or 9 bytes:
### A standard type header
| 0     | 1     | 2     | 3     | 4     |
| :---: | :---: | :---: | :---: | :---: |
| (char) type  | (int) length | (int) length | (int) length | (int) length |
### A node type header
| 0     | 1     | 2     | 3     | 4     | 5     | 6     | 7     | 8     |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| (char) type  | (int) length | (int) length | (int) length | (int) length | (int) special | (int) special | (int) special | (int) special |

- The type is one of the masks above, PLUS 0x80 if:
	+ anonymous node (a node without an identifier)
	+ unsigned byte
	+ unsigned int
- The length is the byte length of the data, including the header.
- The special integer is currently only used by the node and represents how many children a node has. __It is not included in the type header unless the datatype is a node.__ All non-node types only have a type header of length 5.
- Integers are bitshifted and split into char types, which are then fed into the file buffer in a big-endian order.

Nodes are special because they behave like any vector would, but can also be referenced by an identifier. If the node is not anonymous, then the first member of the node is a string type of the identifier. If the member has an identifier it is accessible by that identifier, provided it is unique. The library does not check if the identifier IS unique, so you will have to check that yourself. Should the node be anonymous then it is only accessible by its index, which every object has.

## Using the library
I have provided a concrete example in the "example" folder.

### Background
The basic premise is that a `BinaryMap` object owns a root node. To this node you can `push_back` any type _pointer_ extending `btree_member`, where "member" can be substituted for any type listed above. A `btree_node` can perform many of the same actions that a vector can (since its storage facility is a vector), such as `erase`, `push_back` and `pop_back`. I've re-implemented these inside the btree_node itself for both convenience and to make sure that the underlying pointers are deleted. If you do refer the underlying vector, please make sure you delete any pointer that you erase before you erase it (this is done for you in `btree_node`'s own `erase` functions).

### Setting and retrieving values
It is very simple to retrieve the value from a btree_member using the `getValue` template function and it is equally as easy to set a value using the `setValue` template function.

```cpp
// ...
#include <RPF.h>
using namespace RPF;
// ...
BinaryMap bm;
bm.getRoot()->push_back(new btree_string("My awesome string"));
// Getting a value
string data = bm.getRoot()->at(0)->getValue<string>();
// Setting a value
bm.getRoot()->at(0)->setValue(string("Another awesome string"));
// ...
```

__Note:__ `getValue<string>` is the only way to read a string value and `setValue<string>` is the only way to set a string value. const char* nonsense is not supported.

#### Nodes
A node has a value of type `RPF::node_t`, which is a typedef of `vector<btree_member*>`. You can even set the value of a btree_node, though I would generally suggest re-creating the node for this purpose because I have not tested it substantially. If it works without problems, let me know!

You can initialise a node as anonymous by simply not passing any identifier to the constructor:

```cpp
// ...
#include <RPF.h>
using namespace RPF;
// ...
btree_node* myAnonymousNode = new btree_node(); // Anonymous

btree_node* myIdentifiableNode = new btree_node("my identifier"); // Identifiable
// ...
```

You can of course change the identifier by using the `setIdentifier(string)` member function. If you pass an empty, zero-length string to this function the node will become anonymous. If the string has any other length it will be identifiable. The identifier can be useful in some situations such as erasing a node by using `btree_node::erase(string)` of the parent node or when using `at`, which can accept both an index and an identifier string.

#### Unsigned values
To make a byte (char) or integer unsigned or vice versa, simply ensure that when using `setValue`, your type is of the new desired signedness and the changes will reflect immediately.

### Importing and Exporting
Probably the most important aspect of this library is the ability to export and import RPF files. You can use the `BExporter` and `BImporter` classes to perform this action for you. Both of these classes take a reference to a `BinaryMap` instance in their constructor which they will use to pull data from or populate.

During these processes several exceptions can be thrown:

| Exception | Cause |
| :-------- | :---- |
| time_travel_exception | When the RPF version in the file is different than the parser itself |
| invalid_btree_type_exception | When an invalid btree_type is requested or used (anything but `RPF::btree_type` and the occasional 0x80 mask) |
| corrupted_btree_exception | When the importer incurs an error with the bytes that are read from the raw data or file |
| Any exception from the std | When something goes completely wrong and I haven't caught it |

#### Exporting
You have two ways in which you can export an RPF file once you have constructed a `BExporter`
##### Export to file
Call the `saveFile(string)` function on your exporter object, passing in the path of your desired file.
##### Export to "raw data"
Call the `saveRaw()` function on your exporter object. This will return a `vector<unsigned char>` of the data as it would have been written to the file including version info, etc.
#### Importing
Just like exporting, there are also two ways in which an RPF file can be imported once you have constructed a `BImporter`
##### Import from file
Call the `loadFile(string)` function on your importer object, passing in the path of your desired file.
##### Import from "raw data"
Call the `loadRaw(vector<unsigned char>)` function on your importer object, passing in a vector of the data as it would be exported.

### Arrays / Maps
RPF does not support arrays or maps, but they can be emulated easily. A node is a de-facto array: you can access all of its elements by index. A map can be constructed from a parent node and subnodes which act as the keys. The first index of these subnodes represents the value of the node. In the future I may abstract these concepts into their own classes, but this is not a top priority right now.

## License
This library, example(s) & specification are released under the MIT License of which a copy can be found in the LICENSE file. Accreditation is appreciated.