/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "btree_byte.h"

RPF::btree_byte::btree_byte(char value):value(value){
    
}

RPF::btree_byte::btree_byte(unsigned char value):value(value){
    snd = false;
}

RPF::btree_byte::~btree_byte(){
    
}

unsigned char RPF::btree_byte::getType(){
    return snd ? btree_type::BYTE : (btree_type::BYTE | 0x80);
}

unsigned int RPF::btree_byte::getLength(){
    unsigned int length = (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH);
    length += 1;
    return length;
}

unsigned int RPF::btree_byte::typeSpecific(){
    return 0;
}

vector<unsigned char> RPF::btree_byte::getAsBytes(){
    vector<unsigned char> m = getHeaderBytes();
    m.push_back((unsigned int)value);
    return m;
}

char* RPF::btree_byte::getBYTEValue(){
    return &value;
}

unsigned char* RPF::btree_byte::getUBYTEValue(){
    return reinterpret_cast<unsigned char*>(&value);
}

void RPF::btree_byte::setBYTEValue(void* val){
    *(&value) = *((char*)val);
    snd = true;
}

void RPF::btree_byte::setUBYTEValue(void* val){
    *(&value) = *((unsigned char*)val);
    snd = false;
}