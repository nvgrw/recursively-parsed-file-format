/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

	var create_binary_map 					= librpf.cwrap("create_binary_map", "BinaryMap*", [""]);
	var delete_binary_map 					= librpf.cwrap("delete_binary_map", "", ["BinaryMap*"]);
	var get_binary_map_root 				= librpf.cwrap("get_binary_map_root", "btree_node*", ["BinaryMap*"]);
	var export_binary_map					= librpf.cwrap("export_binary_map", "buffer_t*", ["BinaryMap*"]);
	var import_binary_map					= librpf.cwrap("import_binary_map", "", ["BinaryMap*", "buffer_t*"]);

	var create_buffer						= librpf.cwrap("create_buffer", "buffer_t*", [""]);
	var delete_buffer						= librpf.cwrap("delete_buffer", "", ["buffer_t*"]);
	var buffer_push_back					= librpf.cwrap("buffer_push_back", "", ["buffer_t*", "number"]);
	var buffer_pop_back						= librpf.cwrap("buffer_pop_back", "", ["buffer_t*"]);
	var buffer_at							= librpf.cwrap("buffer_at", "number", ["buffer_t*", "number"]);
	var buffer_erase						= librpf.cwrap("buffer_erase", "", ["buffer_t*", "number"]);
	var buffer_clear						= librpf.cwrap("buffer_clear", "", ["buffer_t*"]);
	var buffer_size							= librpf.cwrap("buffer_size", "number", ["buffer_t*"]);

	var create_btree_node 					= librpf.cwrap("create_btree_node", "btree_node*", ["string"]);
	var create_anonymous_btree_node 		= librpf.cwrap("create_btree_node", "btree_node*", [""]);
	var create_signed_btree_byte 			= librpf.cwrap("create_signed_btree_byte", "btree_byte*", ["number"]);
	var create_unsigned_btree_byte 			= librpf.cwrap("create_unsigned_btree_byte", "btree_byte*", ["number"]);
	var create_btree_float					= librpf.cwrap("create_btree_float", "btree_float*", ["number"]);
	var create_signed_btree_int 			= librpf.cwrap("create_signed_btree_int", "btree_int*", ["number"]);
	var create_unsigned_btree_int 			= librpf.cwrap("create_unsigned_btree_int", "btree_int*", ["number"]);
	var create_btree_string					= librpf.cwrap("create_btree_string", "btree_string*", ["string"]);

	var get_btree_node_identifier			= librpf.cwrap("get_btree_node_identifier", "string", ["btree_node*"]);
	var get_btree_signed_byte_value			= librpf.cwrap("get_btree_signed_byte_value", "number", ["btree_member*"]);
	var get_btree_unsigned_byte_value		= librpf.cwrap("get_btree_unsigned_byte_value", "number", ["btree_member*"]);
	var get_btree_float_value				= librpf.cwrap("get_btree_float_value", "number", ["btree_member*"]);
	var get_btree_signed_int_value			= librpf.cwrap("get_btree_signed_int_value", "number", ["btree_member*"]);
	var get_btree_unsigned_int_value		= librpf.cwrap("get_btree_unsigned_int_value", "number", ["btree_member*"]);
	var get_btree_string_value				= librpf.cwrap("get_btree_string_value", "string", ["btree_member*"]);

	var set_btree_node_identifier			= librpf.cwrap("set_btree_node_identifier", "", ["btree_node*", "string"]);
	var set_btree_signed_byte_value			= librpf.cwrap("set_btree_signed_byte_value", "", ["btree_member*", "number"]);
	var set_btree_unsigned_byte_value		= librpf.cwrap("set_btree_unsigned_byte_value", "", ["btree_member*", "number"]);
	var set_btree_float_value				= librpf.cwrap("set_btree_float_value", "", ["btree_member*", "number"]);
	var set_btree_signed_int_value			= librpf.cwrap("set_btree_signed_int_value", "", ["btree_member*", "number"]);
	var set_btree_unsigned_int_value		= librpf.cwrap("set_btree_unsigned_int_value", "", ["btree_member*", "number"]);
	var set_btree_string_value				= librpf.cwrap("set_btree_string_value", "", ["btree_member*", "string"]);

	var btree_node_push_back				= librpf.cwrap("btree_node_push_back", "", ["btree_node*", "btree_member*"]);
	var btree_node_pop_back					= librpf.cwrap("btree_node_pop_back", "", ["btree_node*"]);
	var btree_node_erase_at					= librpf.cwrap("btree_node_erase_at", "", ["btree_node*", "number"]);
	var btree_node_erase_node_by_key		= librpf.cwrap("btree_node_erase_node_by_key", "", ["btree_node*", "string"]);
	var btree_node_size						= librpf.cwrap("btree_node_size", "number", ["btree_node*"]);
	var btree_node_at 						= librpf.cwrap("btree_node_at", "btree_member*", ["btree_node*", "number"]);

	var btree_node_at_node_by_key			= librpf.cwrap("btree_node_at_node_by_key", "btree_member*", ["btree_node*", "string"]);
	var get_type 							= librpf.cwrap("get_type", "number", ["btree_member*"]);
	var get_length							= librpf.cwrap("get_length", "number", ["btree_member*"]);
	var get_type_specific					= librpf.cwrap("get_type_specific", "number", ["btree_member*"]);

	var delete_btree_member					= librpf.cwrap("delete_btree_member", "", ["btree_member*"]);

	var btree_type = {
	    NODE : 1,
	    NODE_ANONYMOUS : 1 | 0x80,
	    BYTE : 2,
	    BYTE_UNSIGNED : 2 | 0x80,
	    INT : 3,
	    INT_UNSIGNED : 3 | 0x80,
	    STRING : 4,
	    FLOAT : 5,
	}

	function toBuffer(array){
		var buffer = create_buffer();
		for(var i = 0; i < array.length; i++){
			var value = array[i];
			if(typeof value == "number" && value < 256){
				if(value > 128) value -= 256;
				buffer_push_back(buffer, array[i]);	
			}else{
				delete_buffer(buffer);
				throw "Array contains incorrect data";
				return;
			}
		}
		return buffer;
	}

	function toArray(buffer){
		var array = [];
		var size = buffer_size(buffer);
		for(var i = 0; i < size; i++){
			var val = buffer_at(buffer, i);
			if(val < 0) val += 256;
			array.push(val);
		}
		return array;
	}

	function addMemberAttributes(member){
		member.getType = function(){
			return get_type(member.internal);
		}

		member.getLength = function(){
			return get_length(member.internal);
		}

		member.getTypeSpecific = function(){
			return get_type_specific(member.internal);
		}

		member.destroy = function(){
			delete_btree_member(member.internal);
		}
	}

	function assembleForType(item, type){
		var unsignedType = type;
		if(unsignedType < 0){
			unsignedType += 256;
		}
		switch(unsignedType){
			case btree_type.NODE:
			case btree_type.NODE_ANONYMOUS:
				return new BtreeNode(null, item);
			case btree_type.BYTE:
			case btree_type.BYTE_UNSIGNED:
				return new BtreeByte(null, item);
			case btree_type.INT:
			case btree_type.INT_UNSIGNED:
				return new BtreeInt(null, item);
			case btree_type.STRING:
				return new BtreeString(null, item);
			case btree_type.FLOAT:
				return new BtreeFloat(null, item);
			default:
				throw "Invalid type " + type;
		}
	}

	function BtreeByte(valuei, internali){
		var self = this;
		var value = valuei || 0;
		if(typeof value != "number" || value > 0xFF || value < 0) throw "Invalid value for type.";
		self.internal = internali || create_unsigned_btree_byte(value); // Handling unsigned only for now.

		addMemberAttributes(self);

		self.getValue = function(){
			value = get_btree_unsigned_byte_value(self.internal);
			return value;
		}

		self.setValue = function(newValue){
			value = newValue;
			if(typeof value != "number" || value > 0xFF || value < 0) throw "Invalid value for type.";
			set_btree_unsigned_byte_value(self.internal, newValue);
		}
	}

	function BtreeFloat(valuei, internali){
		var self = this;
		var value = valuei || 0.0;
		if(typeof value != "number") throw "Invalid value for type.";
		self.internal = internali || create_btree_float(value);

		addMemberAttributes(self);

		self.getValue = function(){
			value = get_btree_float_value(self.internal);
			return value;
		}

		self.setValue = function(newValue){
			value = newValue;
			if(typeof value != "number") throw "Invalid value for type.";
			set_btree_float_value(self.internal, newValue);
		}
	}

	function BtreeInt(valuei, internali){
		var self = this;
		var value = valuei || 0;
		if(typeof value != "number" || value < -0x80000000 || value > 0x7fffffff) throw "Invalid value for type.";
		self.internal = internali || create_signed_btree_int(value); // Handling signed only for now as JS doesn't support unsigned types.

		addMemberAttributes(self);

		self.getValue = function(){
			value = get_btree_signed_int_value(self.internal);
			return value;
		}

		self.setValue = function(newValue){
			value = newValue;
			if(typeof value != "number" || value < -0x80000000 || value > 0x7fffffff) throw "Invalid value for type.";
			set_btree_signed_int_value(self.internal, newValue);
		}
	}

	function BtreeString(valuei, internali){
		var self = this;
		var value = valuei || "";
		if(typeof value != "string") throw "Invalid value for type.";
		self.internal = internali || create_btree_string(value);

		addMemberAttributes(self);

		self.getValue = function(){
			value = get_btree_string_value(self.internal);
			return value;
		}

		self.setValue = function(newValue){
			value = newValue;
			if(typeof value != "string") throw "Invalid value for type.";
			set_btree_string_value(self.internal, newValue);
		}
	}

	function BtreeNode(identifieri, internali){
		var self = this;
		var identifier = identifieri || "_n_anonymous";
		if(typeof identifier != "string") identifier = "_n_anonymous";
		self.internal = internali || (identifier == "_n_anonymous" ? create_anonymous_btree_node() : create_btree_node(identifier));
		identifier = get_btree_node_identifier(self.internal);

		addMemberAttributes(self);

		self.getIdentifier = function(){
			identifier = get_btree_node_identifier(self.internal);
			return identifier;
		}

		self.setIdentifier = function(newIdentifier){
			identifier = newIdentifier;
			set_btree_node_identifier(self.internal, newIdentifier);
		}

		self.getMembers = function(){
			var size = btree_node_size(self.internal);
			var members = [];
			for(var i = 0; i < size; i++){
				var btreem = btree_node_at(self.internal, i);
				members.push(assembleForType(btreem, get_type(btreem)));
			}
			return members;
		}

		self.at = function(index){
			if(typeof index == "number"){
				var item = btree_node_at(self.internal, index);
				if(item == 0) throw "No item at index " + index;
				return assembleForType(item, get_type(item))
			}else if(typeof index == "string"){
				var item = btree_node_at_node_by_key(self.internal, index);
				if(item == 0) throw "No item at index " + index;
				return assembleForType(item, get_type(item))
			}else{
				throw "Invalid index type supplied.";
			}
		}

		self.erase = function(index){
			if(typeof index == "number"){
				btree_node_erase_at(self.internal, index);
			}else if(typeof index == "string"){
				btree_node_erase_node_by_key(self.internal, index);
			}else{
				throw "Invalid index type supplied.";
			}
		}

		self.push = function(member){
			btree_node_push_back(self.internal, member.internal);
		}

		self.pop = function(){
			btree_node_pop_back(self.internal);
		}

		self.size = function(){
			return btree_node_size(self.internal);
		}
	}

	function BinaryMap(){
		var self = this;
		self.internal = create_binary_map();
		var rooti = get_binary_map_root(self.internal);
		var root = assembleForType(rooti, get_type(rooti));

		self.getRoot = function(){
			return root;
		}

		self.export = function(){
			var buffer = export_binary_map(self.internal);
			var array = toArray(buffer);
			delete_buffer(buffer);
			return array;
		}

		self.import = function(array){
			var buffer = toBuffer(array);
			import_binary_map(self.internal, buffer);
			delete_buffer(buffer);
		}

		self.destroy = function(){
			delete_binary_map(self.internal);
		}

		if(_RPF_IS_NOT_BROWSER){ // Assuming we're dealing with Node.js
			var fs = require("fs");

			function arrayToNodeBuffer(array){
				return new Buffer(array, "utf-8");
			}

			function nodeBufferToArray(buffer){
				var array = [];
				for(var i = 0; i < buffer.length; i++){
					array.push(buffer[i]);
				}
				return array;
			}

			self.exportFile = function(filename, callback){
				fs.writeFile(filename, arrayToNodeBuffer(self.export()), {
					encoding : "utf8"
				}, callback);
			}

			self.importFile = function(filename, callback){
				var self = self;
				fs.readFile(filename, function(err, data){
					self.import(nodeBufferToArray(data));

					callback(err);
				});
			}

			self.exportFileSync = function(filename){
				fs.writeFileSync(filename, arrayToNodeBuffer(self.export()), {
					encoding : "utf8"
				});
			}

			self.importFileSync = function(filename){
				self.import(nodeBufferToArray(fs.readFileSync(filename)));
			}

		}
	}

	return {
		BinaryMap : BinaryMap,
		Node : BtreeNode,
		String : BtreeString,
		Int : BtreeInt,
		Float : BtreeFloat,
		Byte : BtreeByte,
		Type : btree_type,
		internal : {  // Only use this if you know what you're doing. No API conveniences here!
			library : librpf, // Access to the exported emscripten Module

			// Access to all the functions available for interaction with the native code.
			create_binary_map : create_binary_map,
			delete_binary_map : delete_binary_map,
			get_binary_map_root : get_binary_map_root,
			export_binary_map : export_binary_map,
			import_binary_map : import_binary_map,
			create_buffer : create_buffer,
			delete_buffer : delete_buffer,
			buffer_push_back : buffer_push_back,
			buffer_pop_back : buffer_pop_back,
			buffer_at : buffer_at,
			buffer_erase : buffer_erase,
			buffer_clear : buffer_clear,
			buffer_size : buffer_size,
			create_btree_node : create_btree_node,
			create_anonymous_btree_node : create_anonymous_btree_node,
			create_signed_btree_byte : create_signed_btree_byte,
			create_unsigned_btree_byte : create_unsigned_btree_byte,
			create_btree_float : create_btree_float,
			create_signed_btree_int : create_signed_btree_int,
			create_unsigned_btree_int : create_unsigned_btree_int,
			create_btree_string : create_btree_string,
			get_btree_node_identifier : get_btree_node_identifier,
			get_btree_signed_byte_value : get_btree_signed_byte_value,
			get_btree_unsigned_byte_value : get_btree_unsigned_byte_value,
			get_btree_float_value : get_btree_float_value,
			get_btree_signed_int_value : get_btree_signed_int_value,
			get_btree_unsigned_int_value : get_btree_unsigned_int_value,
			get_btree_string_value : get_btree_string_value,
			set_btree_node_identifier : set_btree_node_identifier,
			set_btree_signed_byte_value : set_btree_signed_byte_value,
			set_btree_unsigned_byte_value : set_btree_unsigned_byte_value,
			set_btree_float_value : set_btree_float_value,
			set_btree_signed_int_value : set_btree_signed_int_value,
			set_btree_unsigned_int_value : set_btree_unsigned_int_value,
			set_btree_string_value : set_btree_string_value,
			btree_node_push_back : btree_node_push_back,
			btree_node_pop_back : btree_node_pop_back,
			btree_node_erase_at : btree_node_erase_at,
			btree_node_erase_node_by_key : btree_node_erase_node_by_key,
			btree_node_size : btree_node_size,
			btree_node_at : btree_node_at,
			btree_node_at_node_by_key : btree_node_at_node_by_key,
			get_type : get_type,
			get_length : get_length,
			get_type_specific : get_type_specific,
			delete_btree_member : delete_btree_member,
		},
	};
})();

if(_RPF_IS_NOT_BROWSER) module.exports = rpf;