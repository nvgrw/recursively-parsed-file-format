/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "btree_string.h"

RPF::btree_string::btree_string(string value):value(value){
    
}

RPF::btree_string::~btree_string(){
    
}

unsigned char RPF::btree_string::getType(){
    return btree_type::STRING;
}

unsigned int RPF::btree_string::getLength(){
    unsigned int length = (BTREE_HEADER_LENGTH - BTREE_TYPE_SPECIFIC_LENGTH);
    length += (unsigned int)value.length();
    return length;
}

unsigned int RPF::btree_string::typeSpecific(){
    return 0;
}

vector<unsigned char> RPF::btree_string::getAsBytes(){
    vector<unsigned char> m = getHeaderBytes();
    for(unsigned char bytes:value){
        m.push_back(bytes);
    }
    return m;
}

string* RPF::btree_string::getSTRINGValue(){
    return &value;
}

void RPF::btree_string::setSTRINGValue(void* val){
    *(&value) = *((string*)val);
}