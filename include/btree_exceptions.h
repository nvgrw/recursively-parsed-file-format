//
//  timetravel_exception.h
//  Binary Map Library
//
//  Created by notrodash on 23/02/2014.
//  Copyright (c) 2014 notrodash. All rights reserved.
//

#pragma once

#include <exception>

using namespace std;

class time_travel_exception:public exception{
public:
    virtual const char* what() const throw(){
        return "It appears we have travelled time! File version newer than parser.";
    }
};

class invalid_btree_type_exception:public exception{
public:
    const unsigned int type;
    
    invalid_btree_type_exception(unsigned int type):type(type){
        
    }
    
    virtual const char* what() const throw(){
        return string("BTree type: ").append(to_string(type)).append(" invalid!").c_str();
    }
};

class corrupted_btree_exception:public exception{
public:
    virtual const char* what() const throw(){
        return "Read bytes do not match specification. The data may be corrupted.";
    }
};