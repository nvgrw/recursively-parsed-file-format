/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <iostream>
#include <vector>

#include <RPF.h>

using namespace std;
using namespace RPF;

int main(int argc, const char * argv[])
{
    // Create some binary maps
    BinaryMap exmap; // Export map
    BinaryMap immap, rimmap; // Import map
    
    // Populate the export map
    btree_node* secondary1 = new btree_node("secondary1");
    secondary1->push_back(new btree_string("Hello there"));
    secondary1->push_back(new btree_int((unsigned int)4294961295));
    secondary1->push_back(new btree_int((int)4294961295));
    exmap.getRoot()->push_back(secondary1);
    btree_node* secondary2 = new btree_node(); // This can be called "secondary2" or we can simply make it an anonymous node and refer to it by its index.
    secondary2->push_back(new btree_float(3.14159f));
    secondary2->push_back(new btree_string("Hello again"));
    secondary2->push_back(new btree_byte((char)6));
    exmap.getRoot()->push_back(secondary2);
    btree_node* tertiary1 = new btree_node("tertiary1");
    tertiary1->push_back(new btree_float(14.1552f));
    secondary2->push_back(tertiary1);

    // Just to check
    cout << "Here is what you saved: " << *exmap.getRoot()->at("secondary1")->at(0)->getValue<string>() << "\n";
    cout << "Here is what you saved: " << *exmap.getRoot()->at("secondary1")->at(1)->getValue<unsigned int>() << "\n";
    cout << "Here is what you saved: " << *exmap.getRoot()->at("secondary1")->at(2)->getValue<int>() << "\n";
    cout << "Here is what you saved: " << *exmap.getRoot()->at(1)->at(0)->getValue<float>() << "\n";
    cout << "Here is what you saved: " << *exmap.getRoot()->at(1)->at(1)->getValue<string>() << "\n";
    cout << "Here is what you saved: " << (int)*exmap.getRoot()->at(1)->at(2)->getValue<char>() << "\n";
    cout << "Here is what you saved: " << *exmap.getRoot()->at(1)->at("tertiary1")->at(0)->getValue<float>() << "\n";
    
    // Create an exporter and save the BinaryMap to an RPF file.
    BExporter ex(exmap);
    ex.saveFile("test.rpf");
    
    // Create an importer and load the BinaryMap from an RPF file.
    BImporter im(immap);
    im.loadFile("test.rpf");
    
    // Just to check
    cout << "Here is what I read: " << *immap.getRoot()->at("secondary1")->at(0)->getValue<string>() << "\n";
    cout << "Here is what I read: " << *immap.getRoot()->at("secondary1")->at(1)->getValue<unsigned int>() << "\n";
    cout << "Here is what I read: " << *immap.getRoot()->at("secondary1")->at(2)->getValue<int>() << "\n";
    cout << "Here is what I read: " << *immap.getRoot()->at(1)->at(0)->getValue<float>() << "\n";
    cout << "Here is what I read: " << *immap.getRoot()->at(1)->at(1)->getValue<string>() << "\n";
    cout << "Here is what I read: " << (int)*immap.getRoot()->at(1)->at(2)->getValue<char>() << "\n";
    cout << "Here is what I read: " << *immap.getRoot()->at(1)->at("tertiary1")->at(0)->getValue<float>() << "\n";
    
    // We can also read everything manually
    BExporter rex(exmap);
    vector<unsigned char> data = rex.saveRaw();
    BImporter rim(rimmap);
    rim.loadRaw(data);
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at("secondary1")->at(0)->getValue<string>() << "\n";
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at("secondary1")->at(1)->getValue<unsigned int>() << "\n";
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at("secondary1")->at(2)->getValue<int>() << "\n";
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at(1)->at(0)->getValue<float>() << "\n";
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at(1)->at(1)->getValue<string>() << "\n";
    cout << "Here is what I read from the raw data: " << (int)*rimmap.getRoot()->at(1)->at(2)->getValue<char>() << "\n";
    cout << "Here is what I read from the raw data: " << *rimmap.getRoot()->at(1)->at("tertiary1")->at(0)->getValue<float>() << "\n";
    return 0;
}