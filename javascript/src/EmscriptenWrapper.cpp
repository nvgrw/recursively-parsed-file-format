/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Niklas Vangerow
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "RPF.h"
using namespace std;

extern "C" {
	using namespace RPF;
    typedef vector<unsigned char> buffer_t;

	// Binary Map
    BinaryMap* create_binary_map(){
    	return new BinaryMap();
    }

    btree_node* get_binary_map_root(BinaryMap* map){
    	return map->getRoot();
    }

    // Export
    buffer_t* export_binary_map(BinaryMap* map){
    	BExporter expo(*map);
    	buffer_t* v = new buffer_t(expo.saveRaw());
    	return v;
    }

    // Importer
    void import_binary_map(BinaryMap* map, buffer_t* raw){
    	BImporter impo(*map);
        try{
        	impo.loadRaw(*raw);
        }catch(corrupted_btree_exception e){
            cout << e.what() << endl;
        }
    }

    // Buffer
    buffer_t* create_buffer(){
        return new buffer_t();
    }

    void delete_buffer(buffer_t* buffer){
        delete buffer;
    }

    void buffer_push_back(buffer_t* buffer, unsigned char data){
        buffer->push_back(data);
    }

    void buffer_pop_back(buffer_t* buffer){
        buffer->pop_back();
    }

    unsigned char buffer_at(buffer_t* buffer, size_t index){
        return buffer->at(index);
    }

    void buffer_erase(buffer_t* buffer, size_t index){
        buffer->erase(buffer->begin() + index);
    }

    void buffer_clear(buffer_t* buffer){
        buffer->clear();
    }

    size_t buffer_size(buffer_t* buffer){
        return buffer->size();
    }

    // Create btree_node
    btree_node* create_btree_node(const char* identifier){
    	return new btree_node(string(identifier));
    }

    btree_node* create_anonymous_btree_node(){
    	return new btree_node();
    }

    void set_btree_node_identifier(btree_node* node, const char* identifier){
    	node->setIdentifier(string(identifier));
    }

    const char* get_btree_node_identifier(btree_node* node){
    	return node->getIdentifier().c_str();
    }

    // Node Vector Abstractions
    void btree_node_push_back(btree_node* node, btree_member* member){
    	node->push_back(member);
    }

    void btree_node_pop_back(btree_node* node){
    	node->pop_back();
    }

    void btree_node_erase_at(btree_node* node, size_t position){
    	node->erase(position);
    }

    void btree_node_erase_node_by_key(btree_node* node, const char* key){
    	node->erase(string(key));
    }

    size_t btree_node_size(btree_node* node){
    	return node->size();
    }

    btree_member* btree_node_at(btree_node* node, size_t index){
    	return node->at(index);
    }

    btree_member* btree_node_at_node_by_key(btree_node* node, const char* key){
    	return node->at(string(key));
    }

    // Every member
    unsigned char get_type(btree_member* member){
    	return member->getType();
    }

    unsigned int get_length(btree_member* member){
    	return member->getLength();
    }

    unsigned int get_type_specific(btree_member* member){
    	return member->typeSpecific();
    }

    // Create btree_members
    btree_byte* create_signed_btree_byte(char val){
    	return new btree_byte(val);
    }

    btree_byte* create_unsigned_btree_byte(unsigned char val){
    	return new btree_byte(val);
    }

    btree_float* create_btree_float(float val){
    	return new btree_float(val);
    }

    btree_int* create_signed_btree_int(int val){
    	return new btree_int(val);
    }

    btree_int* create_unsigned_btree_int(unsigned int val){
    	return new btree_int(val);
    }

    btree_string* create_btree_string(const char* val){
    	return new btree_string(string(val));
    }

    // Get values of btree_members
    char get_btree_signed_byte_value(btree_member* member){
    	return *member->getValue<char>();
    }

    unsigned char get_btree_unsigned_byte_value(btree_member* member){
    	return *member->getValue<unsigned char>();
    }

    float get_btree_float_value(btree_member* member){
    	return *member->getValue<float>();
    }

    int get_btree_signed_int_value(btree_member* member){
    	return *member->getValue<int>();
    }

    unsigned int get_btree_unsigned_int_value(btree_member* member){
    	return *member->getValue<unsigned int>();
    }

    const char* get_btree_string_value(btree_member* member){
    	return member->getValue<string>()->c_str();
    }

    // Set values of btree_members
    void set_btree_signed_byte_value(btree_member* member, char value){
    	member->setValue<char>(value);
    }

    void set_btree_unsigned_byte_value(btree_member* member, char value){
    	member->setValue<unsigned char>(value);
    }

    void set_btree_float_value(btree_member* member, float value){
    	member->setValue<float>(value);
    }

    void set_btree_signed_int_value(btree_member* member, int value){
    	member->setValue<int>(value);
    }

    void set_btree_unsigned_int_value(btree_member* member, unsigned int value){
    	member->setValue<unsigned int>(value);
    }

    void set_btree_string_value(btree_member* member, const char* value){
    	member->setValue<string>(string(value));
    }

    // Delete stuff
    void delete_binary_map(BinaryMap* bm){
    	delete bm;
    }

    void delete_btree_member(btree_member* bm){
    	delete bm;
    }
}